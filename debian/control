Source: python-crc32c
Section: python
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all-dev,
 python3-setuptools,
 python3-pytest <!nocheck>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/python-crc32c.git
Vcs-Browser: https://salsa.debian.org/debian/python-crc32c
Homepage: https://github.com/ICRAR/crc32c
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-crc32c
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: python implementation in hardware and software of crc32c
 crc32c is a python package
 implementing the crc32c checksum algorithm.
 It automatically chooses between a hardware-based implementation
 (using the CRC32C SSE 4.2 instruction of Intel CPUs),
 or a software-based one when no hardware support can be found.
 .
 A cyclic redundancy check (CRC) is an error-detecting code
 commonly used in digital networks and storage devices
 to detect accidental changes to raw data.
