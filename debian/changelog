python-crc32c (2.6-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * build-depend on python3-pytest

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 13 Aug 2024 13:27:22 +0200

python-crc32c (2.4.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Jul 2024 08:03:24 +0200

python-crc32c (2.3-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info:
    + use field Reference (not License-Reference)
    + update coverage
  * declare compliance with Debian Policy 4.6.1
  * use debhelper compatibility level 13 (not 12)
  * update git-buildpackage config:
    + use DEP-14 git branches
    + enable automatic DEP-14 branch name handling
    + add usage config

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Sep 2022 15:36:38 +0200

python-crc32c (2.2-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch adopted upstream
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Nov 2020 21:25:39 +0100

python-crc32c (2.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Patch: HWCAP_CRC32 is already defined on arm64. Fixes FTBFS. Closes: #972217

 -- Stefano Rivera <stefanor@debian.org>  Sat, 17 Oct 2020 19:47:59 -0700

python-crc32c (2.1-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * enable autopkgtest
  * use debhelper compatibility level 12 (not 13)
    to ease backporting
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Oct 2020 16:49:26 +0200

python-crc32c (2.0.1-2) unstable; urgency=medium

  * release to unstable

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 16 Jul 2020 11:12:01 +0200

python-crc32c (2.0.1-1) experimental; urgency=low

  * initial packaging
    closes: bug#964856

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 11 Jul 2020 11:18:20 +0200
